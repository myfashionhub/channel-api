Rails.application.routes.draw do
  root 'welcome#index'
  get '/api' => 'welcome#api'
  get '/channel-manager' => 'channels#manager'
  get '/account' => 'users#index'

  resources :sessions, only: [:index, :create]
  get '/logout' => 'sessions#destroy'

  resources :users, except: [:new] do
    resources :api_keys, only: [:index, :create, :destroy]
  end

  namespace :api do
    scope module: :v1 do
      resources :channels
      resources :categories
      get 'curator-video' => 'base#curator_video'
    end
  end
end
