class Category < ActiveRecord::Base
  validates :name, uniqueness: true
  has_and_belongs_to_many :channels

  def self.api(params)
    if params[:name]
      Category.find_channels(params[:name]) 
    else
      Category.all 
    end  
  end

  def self.find_channels(category_params)
    if category_params.include?(',')
      category_names = category_params.downcase.split(',')   
      channels = category_names.map do |category_name|
        Category.find_by(name: category_name).channels
      end
      channels.flatten!
    elsif category_params.include?(' ')
      category_names = category_params.downcase.split(' ')
      search_categories = category_names.map do |category_name|
        Category.find_by(name: category_name)
      end
      channels = Channel.all.select do |channel|
        channel if (search_categories - channel.categories.to_a).empty?
      end
    else 
      category_name = category_params
      channels = Category.find_by(name: category_name).channels
    end
    return channels
  end

end