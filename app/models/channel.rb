require 'will_paginate/array'

class Channel < ActiveRecord::Base
  serialize :category_array, Array
  has_and_belongs_to_many :categories

  def self.create_or_update(params)
    channel = Channel.find_by(href: params[:href])
    if channel.nil?
      channel = Channel.create(
        name: params[:name],
        src: params[:src],
        href: params[:href],
        category_array: params[:category_array],
        desc: params[:desc],
        keywords: params[:keywords],
        mobile_banner: params[:mobile_banner],
        banner_img: params[:banner_img],
        community_site_nid: params[:community_site_nid],
        twitter_widget_id: params[:twitter_widget_id],
        curator_name: params[:curator_name],
        curator_img: params[:curator_img],
        curator_desc: params[:curator_desc],
        curator_video: params[:curator_video]
      )
    else
      params[:id] = channel.id
      Channel.update_with_id(params)
    end

    Channel.add_to_category(channel)
    return channel
  end

  def self.update_with_id(params)
    channel = Channel.find(params[:id])
    channel.update(
      name: params[:name],
      src: params[:src],
      href: params[:href],
      category_array: params[:category_array],
      desc: params[:desc],
      keywords: params[:keywords],
      mobile_banner: params[:mobile_banner],
      banner_img: params[:banner_img],
      twitter_widget_id: params[:twitter_widget_id],
      curator_name: params[:curator_name],
      curator_img: params[:curator_img],
      curator_desc: params[:curator_desc],
      curator_video: params[:curator_video]
    )
    channel.save
    Channel.add_to_category(channel)

    return channel
  end

  def self.add_to_category(channel)
    Category.all.each do |category|
      if channel.category_array.include?(category.name)
        unless category.channels.include?(channel)
          category.channels << channel
        end
      end
    end
  end

  def self.api(params)
    if params[:category]
      channels = Category.find_channels(params[:category])
    elsif params[:term]
      channels = Channel.all.where("name LIKE '%#{params[:term]}%' OR href LIKE '%#{params[:term]}%'")
    else
      channels = Channel.all
    end

    page     = params[:page] || 1
    per_page = (params[:per_page] || 20).to_f
    total    = channels.length.to_f
    num_pages= (total/per_page).ceil

    if params[:pagination] == 'false'
      return {channels: channels, total: total}
    else
      return { channels: channels.paginate(page: page, per_page: per_page),
        pagination: {total: total, page: page, per_page: per_page, num_pages: num_pages} }
    end
  end

end
