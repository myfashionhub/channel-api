class User < ActiveRecord::Base
  self.table_name = 'waywiremanager_users'
  authenticates_with_sorcery!
  has_many :api_keys

  def self.create_new(params)
    puts "user create new #{params}"
    user     = User.new(params)
    email    = params[:email].downcase
    password = params[:password]

    if User.find_by(email: email)
      msg    = "Email already taken."
      status = 'error'
    elsif user.save
      msg    = 'Successfully signed up.'
      status = 'success'    
    end    
    { msg: msg, status: status }
  end

end

