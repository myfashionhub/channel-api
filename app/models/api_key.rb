require 'securerandom'

class ApiKey < ActiveRecord::Base
  self.table_name = 'waywiremanager_api_keys'
  belongs_to :user
  before_create :generate_access_token

  private 
  def generate_access_token
    begin 
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token) 
  end
end

# rails cast http://railscasts.com/episodes/352-securing-an-api?view=asciicast