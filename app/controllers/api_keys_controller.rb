class ApiKeysController < ApplicationController
  
  def index
    api_keys = ApiKey.where(user_id: params[:user_id])
    render json: api_keys.to_json
  end

  def create
    api_key = ApiKey.create(user_id: params[:user_id])
    render json: { msg: 'Successfully generate new API key.' }
  end

  def destroy
    ApiKey.destroy(params[:id])
    render json: { msg: 'Key deleted.' }
  end

end  

=begin
    user_api_keys GET    /users/:user_id/api_keys(.:format)     api_keys#index
                  POST   /users/:user_id/api_keys(.:format)     api_keys#create
     user_api_key DELETE /users/:user_id/api_keys/:id(.:format) api_keys#destroy
=end