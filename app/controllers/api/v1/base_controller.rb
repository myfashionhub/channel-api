class Api::V1::BaseController < ApplicationController
  before_filter :restrict_access
  
  def curator_video
    url = params[:url]
    result = JSON.parse(HTTParty.get(url))
    uuid = result['clips'][0]['uuid']
    video_url = 'https://cameratag.com/videos/'+uuid+'/qvga/mp4.mp4'
    render json: video_url.to_json
  end

  def curator_video
    url = params[:url]
    result = JSON.parse(HTTParty.get(url))
    uuid = result['clips'][0]['uuid']
    video_url = 'https://cameratag.com/videos/'+uuid+'/qvga/mp4.mp4'
    render json: video_url.to_json
  end


  private
  def restrict_access
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      puts "TOKEN #{token}"
      ApiKey.exists?(access_token: token) 
    end
  end
<<<<<<< HEAD
end
=======

  def render_unauthorized
    self.headers['WWW-Authenticate'] = 'Token realm="Application"'
    render json: { msg: '401: Bad credentials' }
  end

end

# Securing API: http://railscasts.com/episodes/352-securing-an-api?view=asciicast
>>>>>>> master
