class Api::V1::CategoriesController < Api::V1::BaseController
  
  def index
    categories = Category.api(params)
    render json: categories.to_json
  end

  def create
    Category.find_or_create_by(name: params[:name])
  end

  def show
    category = Category.find(params[:id])
    render json: category.channels.to_json
  end

  def update
  end

  def destroy
  end

end