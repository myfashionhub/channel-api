class Api::V1::ChannelsController < Api::V1::BaseController
  
  def manager
  end

  def index
    results = Channel.api(params)

    render json: { channels: results[:channels], 
                   pagination: results[:pagination] }.to_json
  end

  def create
    Channel.create_or_update(params)
    render json: { msg: 'Successfully create channel.' }.to_json
  end

  def show
    channel = Channel.find(params[:id])
    render json: channel.to_json
  end

  def update
    Channel.update_with_id(params)
    render json: { msg: 'Successfully update channel.' }.to_json
  end

  def destroy
    Channel.find(params[:id]).destroy
    render json: { msg: 'Successfully remove channel.' }.to_json
  end

end
