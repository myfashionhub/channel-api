class SessionsController < ApplicationController
  def index
    render json: {user_id: session[:user_id]}
  end

  def create
    user = login(params[:email].downcase, params[:password])
    info = Session.create_new(user, params)
    render json: info.to_json
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path
  end
end
