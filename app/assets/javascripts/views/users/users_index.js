RailsApi.Views.UserView = Backbone.View.extend({
  template: JST['users/index'],

  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    //this.listenTo(this.model, 'destroy', this.remove);
  },

  render: function() {
    var user = this.template(this.model.attributes);
    this.$el.html(user);
    return this;
  },

  events: {
    'click .edit-user': 'showEditForm',
    'click .edit-password': 'editPassword',
    'submit .edit-form': 'updateUser',
    'click .cancel': 'cancelUpdate'
  },

  showEditForm: function(e) {
    e.preventDefault()
    var email = this.$el.find('.email'),
        org = this.$el.find('.organization'),
        editButton = this.$el.find('.edit-user'),
        cancelButton = $('<button>').html('Cancel').attr('class', 'cancel'),
        fields = [];
    fields.push(email, org);

    _.each(fields, function(field) {
      $(field).replaceWith(function() {
        var fieldValue = $(this).html(),
            fieldLabel = $(this).attr('data'),
            fieldName = $(this).attr('class'),
            input = $('<input type="text">'),
            div = $('<div>');
        input.addClass(fieldName).val($.trim(fieldValue)).attr('placeholder', fieldLabel);
        div.append(input);
        return div;
      });
    });

    //this.$el.find('.edit-password').removeClass('hidden');
    editButton.html('Save').attr('class', 'save-user');
    cancelButton.appendTo(this.$el);
    $(this.el).wrapInner($('<form>').addClass('edit-form'));
  },

  // editPassword: function(e) {
  //   e.preventDefault();
  //   $('.password input').removeClass('hidden');
  //   $('.cancel-password').removeClass('hidden');
  //   $('.edit-password').addClass('hidden');
    
  //   $('.cancel-password').click(function(e) {
  //     e.preventDefault();
  //     $('.password input').addClass('hidden');
  //     $('.cancel-password').addClass('hidden');
  //     $('.edit-password').removeClass('hidden');
  //   });
  // },

  updateUser: function(e) {
    e.preventDefault();
    var email = this.$el.find('.email').val(),
        organization = this.$el.find('.organization').val();
        password = this.$el.find('.current-password').val(),
        new_password = this.$el.find('.new-password').val();

    this.model.set({ 
      email: email, organization: organization
    });

    this.model.url = '/users/'+this.model.id;
    this.model.save(null, {
      success: function(data) {
        notify(data.attributes.msg, 'success'); 
      }
    }, {
      error: function(data) {
        notify(data.attributes.msg, 'error'); }
    });
  },

  cancelUpdate: function(e) {
    e.preventDefault();
    this.$el.empty();
    this.render();
  }
  
});