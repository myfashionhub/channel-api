RailsApi.Routers.ApiKeys = Backbone.Router.extend({
  routes: {
  },
  
  initialize: function() {
    var that = this;
    this.fetchApiKeys();
    $('.new-key').click(function(e) { 
      that.generateNewKey(e); 
    });
  },

  fetchApiKeys: function(e) {
    var that = this;
    this.apiKeys = new RailsApi.Collections.ApiKeys({});
    this.apiKeys.url = '/users/'+window.user_id+'/api_keys';

    this.apiKeysView = new RailsApi.Views.ApiKeysView({
      collection: that.apiKeys, 
      el: $('ul.api-keys')
    });

    this.apiKeys.fetch({async: false}).done(function(data) {
      that.apiKeysView.render();
    });
  },

  generateNewKey: function(e) {
    e.preventDefault();
    var that = this;

    if (this.checkKeyLimit()) {
      var newApiKey = new RailsApi.Models.ApiKey({
        user_id: user_id
      });

      newApiKey.url = '/users/'+user_id+'/api_keys';
      newApiKey.save(null, {
        success: function(response) {
          notify(response.attributes.msg, 'success');
          that.apiKeys.fetch();
        },
        error: function(response) {}     
      });
    }
  },

  checkKeyLimit: function() {
    if ($('.api-keys li').length < 5) {
      return true;
    } else {
      notify('You have a maximum of 5 keys.', 'error');
      return false;
    }
  }

}); 