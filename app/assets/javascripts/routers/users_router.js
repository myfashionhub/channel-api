RailsApi.Routers.Users = Backbone.Router.extend({
  routes: {
    'show': 'fetchUserInfo'
  },

  initialize: function() { 
    this.createUser();
    this.login();
    this.user_id = window.user_id;
    this.fetchUserInfo($('#user-info'), '/users/'+window.user_id); 
  },

  fetchUserInfo: function(el, url) {
    var that = this;
    this.user = new RailsApi.Models.User({});
    this.userView = new RailsApi.Views.UserView({
      model: that.user,
      el: el
    });
    this.user.url = url;

    this.user.fetch({async: false}).done(function(data) {
      that.userView.render();
    });
  },

  createUser: function() {
    var that = this;

    $('form.new-user').submit(function(e) {
      e.preventDefault();
      var form = $('form.new-user'),
          organization = form.find('input[name="organization"]').val(),
          email = form.find('input[name="email"]').val(),
          password = form.find('input[name="password"]').val(),
          passwordConf = form.find('input[name="password_confirmation"]').val(); 
      if (that.validateUserInput(email, password, passwordConf)) {
        var newUser = new RailsApi.Models.User({user: 
          { organization: organization, 
            email: email, 
            password: password
          }  
        }); 

        newUser.url = '/users';
        newUser.save(null, {
          success: function(data) {
            notify(data.attributes.msg, 'success'); 
            $('form.new-user input').val(''); }
        }, {
          error: function(data) {
            notify(data.attributes.msg, 'error'); }
        });         
      } 
    });
  },

  validateUserInput: function(email, password, passwordConf) {
    var error = 0,
        errorField = $('form.new-user .error');  
    errorField.empty();

    if (password != passwordConf) {
      errorField.append('<p>Passwords do not match.</p>');
      error += 1;
    } 
    if (email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) === null) {
      errorField.append('<p>Email is invalid.</p>'); 
      error += 1;   
    } 

    if (error === 0) { return true; } 
    else { return false; }
  },

  login: function() {
    var that = this;
    $('form.new-session').submit(function(e) {
      e.preventDefault();
      var email = $('form.new-session').find('input[name="email"]').val(),
          password = $('form.new-session').find('input[name="password"]').val();

      $.ajax({
        url: '/sessions',
        type: 'POST', 
        data: {email: email, password: password},
        success: function(response) {
          if (response.status === 'success') {
            window.location.replace('/account');
            $('form.new-session input').val('');
          } else {
            notify(response.msg, 'error');
          }
        }
      });
    });
  }

});
