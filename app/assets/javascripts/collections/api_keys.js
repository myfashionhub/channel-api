RailsApi.Collections.ApiKeys = Backbone.Collection.extend({
  model: RailsApi.Models.ApiKey,
  url: '/users/:user_id/api_keys'
});
