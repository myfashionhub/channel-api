// Channel manager nav
function toggleSection($target) {
  if ($target.attr('href').indexOf('#') > -1) {
    $('.current').removeClass('current');

    var sectionName = $.trim($target.attr('class').replace('current', ''));
    $target.addClass('current');
    $('section.'+sectionName).addClass('current');
  }
}

function detectSection() {
  var anchor = window.location.hash,
      href   = window.location.href;

  if (anchor === '' && href.indexOf('/channel-manager') > -1) {
    window.location.hash = '#all';
    anchor = '#all';
  } else if (anchor === '' && href.indexOf('/account') > -1) {
    window.location.hash = '#show';
    anchor = '#show';
  } else if (href.indexOf('/channel-manager') === -1 && href.indexOf('/account') === -1) { // Home page
    window.location.hash = '#login';
    anchor = '#login';
  }

  toggleSection($("a[href='"+anchor+"']"));
}

function clearResultButton() {
  if ($('.search-channels .results').children().length > 0) {
    $('.search-channels .clear').addClass('dirty');
  } else {
    $('.search-channels .clear').removeClass('dirty');
  }
}
