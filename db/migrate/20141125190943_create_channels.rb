class CreateChannels < ActiveRecord::Migration
  def up
    create_table :channels do |t|
      t.string :name
      t.string :src
      t.string :href
      t.string :category_array  
      t.text :desc
      t.string :keywords
      t.string :curator_name
      t.string :curator_video
      t.string :curator_img 
      t.text :curator_desc
      t.string :banner_img
      t.string :mobile_banner
      t.string :twitter_widget_id
      t.integer :community_site_nid
      t.timestamps
    end

    create_table :categories do |t|
      t.string :name
      t.timestamps
    end

    create_table :categories_channels do |t|
      t.integer :category_id
      t.integer :channel_id
      t.timestamps
    end
  end
  
  def down
    drop_table :channels
    drop_table :categories
    drop_table :categories_channels
  end
end
