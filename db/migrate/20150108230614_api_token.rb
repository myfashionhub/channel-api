class ApiToken < ActiveRecord::Migration
  def change
    create_table :waywiremanager_api_keys do |t|
      t.string :access_token
      t.string :role
      t.integer :user_id
      t.date :expires_at
      t.timestamps 
    end

    create_table :waywiremanager_users do |t|
      t.string :email, null: false
      t.string :crypted_password, null: false
      t.string :salt, null: false
      t.string :organization      
    end
  end
end
