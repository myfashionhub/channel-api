-- MySQL dump 10.15  Distrib 10.0.15-MariaDB, for Linux (x86_64)
--
-- Host: us-cdbr-iron-east-01.cleardb.net    Database: 
-- ------------------------------------------------------
-- Server version	5.5.40-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `heroku_e7bdd5031f47b17`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `heroku_e7bdd5031f47b17` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `heroku_e7bdd5031f47b17`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'curated','2015-01-02 16:46:03','2015-01-02 16:46:03'),(11,'news','2015-01-02 16:46:03','2015-01-02 16:46:03'),(21,'food','2015-01-02 16:46:03','2015-01-02 16:46:03'),(31,'travel','2015-01-02 16:46:03','2015-01-02 16:46:03'),(41,'health','2015-01-02 16:46:03','2015-01-02 16:46:03'),(51,'fashion','2015-01-02 16:46:03','2015-01-02 16:46:03'),(61,'lifestyle','2015-01-02 16:46:03','2015-01-02 16:46:03'),(71,'entertainment','2015-01-02 16:46:03','2015-01-02 16:46:03'),(81,'music','2015-01-02 16:46:03','2015-01-02 16:46:03'),(91,'sports','2015-01-02 16:46:03','2015-01-02 16:46:03'),(101,'technology','2015-01-02 16:46:03','2015-01-02 16:46:03');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_channels`
--

DROP TABLE IF EXISTS `categories_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=821 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_channels`
--

LOCK TABLES `categories_channels` WRITE;
/*!40000 ALTER TABLE `categories_channels` DISABLE KEYS */;
INSERT INTO `categories_channels` VALUES (1,1,1,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(11,1,11,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(21,1,21,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(31,1,31,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(41,1,41,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(51,1,51,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(61,1,61,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(71,1,71,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(81,1,81,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(91,1,91,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(101,1,101,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(111,1,111,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(121,1,121,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(131,1,131,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(141,1,141,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(151,1,151,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(161,1,161,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(171,1,171,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(181,1,181,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(191,1,191,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(201,1,201,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(211,1,211,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(221,1,221,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(231,1,231,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(241,1,241,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(251,1,251,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(261,1,261,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(271,1,271,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(281,1,281,'2015-01-02 16:46:03','2015-01-02 16:46:03'),(291,1,291,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(301,1,301,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(311,1,311,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(321,1,321,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(331,1,331,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(341,1,341,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(351,1,351,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(361,1,361,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(371,1,371,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(381,1,381,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(391,1,391,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(401,1,401,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(411,1,411,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(421,1,421,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(431,11,1,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(441,11,31,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(451,11,231,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(461,31,401,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(471,41,81,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(481,41,131,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(491,51,221,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(501,61,11,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(511,61,21,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(521,61,41,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(531,61,61,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(541,61,71,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(551,71,51,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(561,71,91,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(571,71,111,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(581,71,141,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(591,71,151,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(601,71,161,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(611,71,171,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(621,71,181,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(631,71,201,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(641,71,261,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(651,71,301,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(661,71,311,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(671,71,321,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(681,71,331,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(691,71,341,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(701,71,351,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(711,81,101,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(721,81,121,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(731,81,211,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(741,81,241,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(751,81,251,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(761,81,271,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(771,81,281,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(781,81,291,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(791,91,381,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(801,101,361,'2015-01-02 16:46:04','2015-01-02 16:46:04'),(811,101,371,'2015-01-02 16:46:04','2015-01-02 16:46:04');
/*!40000 ALTER TABLE `categories_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_array` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curator_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curator_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curator_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_widget_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `curator_video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `community_site_nid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=431 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'Daily Dose','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/DailyDose.png','http://dailydose.waywire.com/','---\n- news\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,165525),(11,'Doable DIY','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Doable-DIY-Chiclet.png','http://doablediy.waywire.com/','---\n- lifestyle\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,166109),(21,'Modern Coffee','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Modern-Coffee-Chiclet.png','http://moderncoffee.waywire.com/','---\n- lifestyle\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,166239),(31,'Hillary Clinton','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/hillaryTV_new.png','http://hillarytv.waywire.com/','---\n- news\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,164990),(41,'Cosplay','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Cosplay-TV-Chiclet.png','http://cosplaytv.waywire.com/','---\n- lifestyle\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,166167),(51,'Best of Broadway','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Best-of-Broadway-Chiclet.png','http://bestofbroadway.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,166113),(61,'Parenting Exceptional Kids','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/ExeptionalKids.png','http://parentingexceptionalkids.waywire.com','---\n- lifestyle\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:02','2015-01-02 16:46:18',NULL,NULL,165499),(71,'Living Green','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/LivingGreen.png','http://livinggreen.waywire.com','---\n- curated\n- lifestyle\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165869),(81,'Body Image','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Body-Image-Chiclet.png','http://bodyimage.waywire.com/','---\n- health\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165775),(91,'Harry Potter','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/HarryPotterTV.png','http://harrypotter.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165797),(101,'Justin Bieber','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/JustinBieberTV.png','http://www.justinbieber.waywire.com','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162548),(111,'Vino Video','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Vino-Video-Chiclet.png','http://vinovideo.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165927),(121,'Miley Cyrus','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/MileyCyrusTV_new.png','http://mileycyrus.waywire.com','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162459),(131,'Alzheimers','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/AlzheimersTVnew.png','http://alzheimerstv.waywire.com/','---\n- health\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165038),(141,'Big Bang Theory','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/bigbantheory.png','http://thebigbangtheory.waywire.com','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162544),(151,'The Walking Dead','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/TheWalkingDead.png','http://thewalkingdead.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162443),(161,'Doctor Who','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/doctorwhoTV.png','http://www.doctorwho.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165507),(171,'Salsa Dancing','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/SalsaDancing.png','http://salsadancing.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165945),(181,'WWE Raw','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/insideWWEb.png','http://wweraw.waywire.com','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162445),(191,'Bark Park','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/BarkParkicon.png','http://barkpark.waywire.com/','---\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165821),(201,'Game of Thrones','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/GameOfThrones.png','http://gameofthrones.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162441),(211,'The Voice','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/TheVoice.png','http://www.thevoice.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162449),(221,'Vintage Style','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/VintageStyle.png','http://vintagestyle.waywire.com/','---\n- fashion\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165847),(231,'Camp Horror','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Camp-Horror-Chiclet.png','http://camphorror.waywire.com/','---\n- news\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,166083),(241,'Lady Gaga','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/LadyGagaTV_new.png','http://www.ladygaga.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162455),(251,'Jay Z','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/JayZtv_New.png','http://jayz.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165060),(261,'Adam Levine','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/adamlevinenew.png','http://www.adamlevine.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162461),(271,'Beyonce','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/beyonceTV_new.png','http://www.beyonce.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162451),(281,'John Legend','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/johnledgen_New.png','http://www.johnlegend.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,164422),(291,'Katy Perry','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/KatyPerryTV_new.png','http://www.katyperry.waywire.com/','---\n- music\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162550),(301,'One Direction','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/One-Direction-Chiclet.png','http://onedirection.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,166215),(311,'Ellen','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/EllenDeGenerous.png','http://www.ellen.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162467),(321,'Jimmy Fallon','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/JimmyFallonnewicon.png','http://www.jimmyfallon.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162463),(331,'Jennifer Lawrence','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/JenniferLawrencenew.png','http://www.jenniferlawrence.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,162465),(341,'Jimmy Kimmel','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/JimmyKimmelnewicon.png','http://www.jimmykimmel.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,163126),(351,'Conan','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/ConanOBrian.png','http://conan.waywire.com/','---\n- entertainment\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,163146),(361,'Diversity in Tech','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/DiversityInTech.png','http://diversityintech.waywire.com/','---\n- technology\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,164048),(371,'Tech Vision','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/TechVisionTV.png','http://techvisiontv.waywire.com/','---\n- technology\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:18',NULL,NULL,165193),(381,'Track & Field','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/Track_FieldTV.png','http://track-fieldtv.waywire.com/','---\n- sports\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:19',NULL,NULL,165149),(391,'NBA','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/NBATV.png','http://nba.waywire.com/','---\n- sport\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:19',NULL,NULL,165519),(401,'Travel Buzz','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/TravelBuzz.png','http://travelbuzz.waywire.com/','---\n- curated\n- travel\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:19',NULL,NULL,165767),(411,'Marijuana','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/MarijuanaTVicon.png','http://marijuanatv.waywire.com/','---\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:19',NULL,NULL,165761),(421,'Environment','http://waywire.com/media/site/YH5PLD0V56XPX2RS/uploads/EnvironmentTV-1.png','http://environment.waywire.com/','---\n- curated\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-01-02 16:46:03','2015-01-02 16:46:19',NULL,NULL,165755);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20141125190943');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-02 11:52:03
