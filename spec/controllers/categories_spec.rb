require 'rails_helper'

RSpec.describe CategoriesController do 
  render_views

  describe 'GET index' do 
    before :each do  
      categories = ['curated', 'news', 'food', 'travel', 'health',
        'fashion', 'lifestyle', 'entertainment', 'music', 'sports',
        'technology'
      ]
      categories.each do |category|
        Category.find_or_create_by(
          name: category
        )
      end       
    end

    it 'should show all categories' do 
      get :index, format: :json 
      results = JSON.parse(response.body)
      results.length.should eq(11)
    end
  end 

  describe 'GET show' do 
    before :each do 
      @category = Category.find_or_create_by(name: 'curated')      
      25.times do
        Channel.create_or_update(
          name: Faker::Lorem.words(1),
          href: Faker::Internet.url('waywire.com'),
          category_array: ['curated']
        )
      end       
    end

    it 'should show all channels in that category' do 
      get :show, id: @category.id
      results = JSON.parse(response.body)
      results.length.should eq(25)
    end
  end 
end  