require 'rails_helper'

RSpec.describe ChannelsController, type: :controller do 
  render_views

  describe 'GET index' do 
    before :each do 
      @dailydose = Channel.create_or_update(
        href: 'dailydose.waywire.com', 
        name: 'Daily Dose'
      )
      25.times do
        Channel.create_or_update(
          href: Faker::Internet.url, 
          name: Faker::Commerce.department
        )
      end       
    end
    
    it 'should be able to return results in json' do 
      get :index, format: :json
      response.header['Content-Type'].should include 'application/json'
      response.code.should eq('200')
    end

    it 'should return all channels if pagination is false' do 
      get :index, format: :json, pagination: 'false'
      results = JSON.parse(response.body)['channels']
      results.length.should eq(26)
    end

    it 'should return 20 channels if params is empty' do 
      get :index, format: :json
      results = JSON.parse(response.body)['channels']
      results.length.should eq(20)
    end  

    it 'should return correct search result' do 
      get :index, format: :json, term: 'Daily Dose'
      result = JSON.parse(response.body)['channels']
      result[0]['name'].should eq('Daily Dose')
    end      
  end
    
  describe 'PUT update' do 
    before :each do 
      @channel = Channel.create_or_update(
        href: 'http://dailydose.waywire.com', 
        name: 'daily dose', 
        category_array: ['curated','news']
      )
    end

    it 'should update channel with correct params' do 
      put :update, { id: @channel.id, name: 'Daily Dose' }
      @channel.reload
      @channel.name.should eq('Daily Dose')
    end
  end

  describe 'POST destroy' do 
    before :each do 
      @channel = Channel.create_or_update(
        href: 'http://dailydose.waywire.com', 
        name: 'daily dose'
      )
    end

    it 'successfully destroys a channel' do 
      get :destroy, id: @channel.id 
      msg = JSON.parse(response.body)['msg']
      msg.should eq('Successfully remove channel.')
      Channel.all.length.should eq(0)
    end
  end

end