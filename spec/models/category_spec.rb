require 'rails_helper'

RSpec.describe Category, type: :model do 

  before :each do 
    @curated = Category.find_or_create_by(name: 'curated')
    @health = Category.find_or_create_by(name: 'health')
    @dailydose = Channel.create_or_update(
      href: 'dailydose.waywire.com',
      name: 'Daily Dose',
      category_array: ['curated', 'news']
    )
    @bodyimage = Channel.create_or_update(
      href: 'bodyimage.waywire.com',
      name: 'Body Image', 
      category_array: ['curated', 'health']
    )    
  end

  it 'has unique name attribute' do 
    Category.find_or_create_by(name: 'curated')
    Category.all.size.should eq(2)
  end

  it 'can store channels via relationship' do 
    # Channel create_or_update already adds to category
    @curated.channels.length.should eq(2)    
  end

  it 'fetches the correct channels (either)' do 
    lifestyle = Category.find_or_create_by(name: 'lifestyle')
    coffee = Channel.create_or_update(
      href: 'moderncoffee.waywire.com',
      category_array: ['food','lifestyle']
    )
    results = Category.find_channels('curated,lifestyle')
    results.length.should eq(3)    
  end

  it 'fetches the correct channels (and)' do
    results = Category.find_channels('curated health')
    results.should include(@bodyimage) 
    results.length.should eq(1)  
  end  
end